#!/bin/bash

#SBATCH -J svalbard-800m

#SBATCH --account 
##SBATCH --account

#SBATCH --mem-per-cpu=2000MB

#SBATCH --nodes=3
#SBATCH --ntasks-per-node=16
#SBATCH --exclusive
#SBATCH --priority=100000
##SBATCH --partition normal
#SBATCH --qos=devel
#SBATCH --time=0-01:00:00

#------------

source /home/$USER/models/metroms/apps/svalbard-800m_nobio/compile_here.sh
datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${METROMS_COMPDIR}/log_${datstamp}_svalbard-800m 2>&1

cp /global/home/$USER/models/metroms/apps/svalbard-800m_nobio/include/svalbard-800m.in ${METROMS_COMPDIR}/roms.in
cp ${METROMS_COMPDIR}/roms.in ${METROMS_COMPDIR}/roms.in_${datstamp}
cp /global/home/$USER/models/metroms/apps/svalbard-800m_nobio/include/svalbard-800m.h ${METROMS_COMPDIR}/svalbard-800m.h_${datstamp}


module load gold/2.1.5.0 StdEnv intel/13.0 netCDF/4.2.1.1-intel-13.0 OpenMPI/1.6.2-intel-13.0

cd ${METROMS_COMPDIR}
echo ${METROMS_COMPDIR}
#cat  ${METROMS_COMPDIR}/roms.in


HOME_PATH=/global/home/$USER/models/metroms/apps/svalbard-800m_nobio 
host_file=${HOME_PATH}/slurm.hosts
srun hostname -s | sort -u > $host_file



if [ ! -f ${host_file}_"slots" ]; then
    :
else
    mv ${host_file}_"slots" ${host_file}_"slots_old"
fi

counter=0
while read p; do                                                                                                                                                                                           
    if [ "$counter" == "0" ]; then
	echo ${p}" slots=8" >> ${host_file}_"slots"
    elif [ "$counter" == "1" ]; then
	echo ${p}" slots=4" >> ${host_file}_"slots"
    else
	echo ${p}" slots=12" >> ${host_file}_"slots"
    fi
    counter=$((counter+1))
done <$host_file 



#mpirun --mca orte_base_help_aggregate 0 -np 24 ${METROMS_COMPDIR}/oceanG /global/work/mitya/tmproms/run/arctic-4km/roms.in
mpirun -machinefile ${host_file}_"slots" ${METROMS_COMPDIR}/oceanG roms.in

